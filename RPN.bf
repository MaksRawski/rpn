GOAL: reuse calc until whole input is consumed
takes 2 digits (without any delimiter between them) and a sign (either plus | minus | * | /)
op cell = cell before a
a = first int
b = second int
sign = sign of the equation third char

if the op cell is 0 it means we don't want to do any more operations
if it's != we execute summing this many times

>+[>,-------------] take in characters till newline (they will be 13 smaller from their actual values)
+[<] go to 2 to the right from the first char
>> a

change a and b into ints
-----------------------------------
>-----------------------------------

> sign

function calc:
puts result into a
[------------------------------ if sign was plus the cell would be 0 here
	<<<[-]+>>> set the op cell to 1
	!TAPE
	[+++++check if not *
	[---- check if not /
		<<<- zero the op cell on /
		[
			[-]  when the sign was different from plus * or / we zero the sign cell as we are going to use it
			<<<+>>> set the op cell to 1
			invert b
			<[->+<] move value to the right
			>[<->-] put inverted value in b
		]
	]
	]
	TODO: ideally we would like to just loop this summation b times when the sign was *
	this might end up being a special case which we will just note with b before the chars
	<[<+>-] sum b into a
]
<. make the result ascii and print it
